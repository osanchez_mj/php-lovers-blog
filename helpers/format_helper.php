<?php 
date_default_timezone_set('America/Mexico_City');
//Format the date


function formatDate($date) {
	return date( 'g:ia \o\n l jS F Y', strtotime($date));
}

function shortenText($text, $chars = 450) {
	
	$text= $text." ";
	//substr devuelve una parte de todo el texto que se le indique;
	$text=substr($text, 0, $chars);
	
	//Posteriormente volvemos a utilizar la misma funci�n para que strrpos busque la ultima ocurrencia " " en este caso y nos regrese toda la cadena
	//y al final solo concatenamos los 3 puntos
	
	$text=substr($text, 0, strrpos($text, " "));
	$text = $text. " ...";
	
	
	return $text;
	
	
	
}
	
?>