<?php include 'config/config.php';?>
<?php include 'libraries/Database.php';?>
<?php include 'helpers/format_helper.php';?>
<?php include 'includes/header.php';?>

<?php 

//crear db object

$db = new Database();

	//check url for category
	
	if (isset($_GET['category'])){
		
		$category = $_GET['category'];
		//Create our query within the get method variable
		
		$query = "SELECT * FROM posts WHERE category=".$category;
		
		//Run Query
		
		$posts = $db->select($query);
	}
	else {
		
		//Create our query
		
		$query = "SELECT * FROM posts ORDER BY id DESC";
		
		//Run Query
		
		$posts = $db->select($query);
	}

	

	//query for categories table
	
	$query = "SELECT * FROM categories";
	
	//Get correct data
	
	$categories= $db -> select($query);
	
	

?>
<body>

    <div class="blog-masthead">
        <div class="container">
            <nav class="blog-nav">
                <a class="blog-nav-item" href="index.php">Home</a>
                <a class="blog-nav-item active" href="posts.php">All Post</a>
            </nav>
        </div>
    </div>

    <div class="container">

        <div class="blog-header">
           <div class="logo">
               <img src="images/logo.png" alt="Logo">
           </div>
            <h1 class="blog-title">PHP Lovers Blog</h1>
            <p class="lead blog-description">PHP News, Tutorials, Videos &amp; More</p>
        </div>
    </div>


   <div class="container">
    <div class="row">

        <div class="col-sm-8 blog-main">
        <?php if ($posts): ?>
        
	        <?php while ($row = $posts->fetch_assoc()):?>
	            <div class="blog-post">
	                <h2 class="blog-post-title"><?php echo $row['title'];?></h2>
	                <p class="blog-post-meta"><?php echo formatDate($row['date']);?><a href="#"><?php echo" ". $row['author'];?></a></p>
	                <p class="text-justify">
	                    <?php echo $row['body'];?>
	                </p>
		            </div>
	            <!-- /.blog-post -->
        	<?php endwhile;?>

		<?php else :?>
			<p>There are no posts yet</p>
		<?php endif;?>
        </div>
        <!-- /.blog-main -->
        
        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    <div class="sidebar-module sidebar-module-inset">
    	<?php echo $aboutContent;?>
    </div>
    <div class="sidebar-module">
        <h4>Categories</h4>
        
        <?php if ($categories):?>
        <ol class="list-unstyled">
        
        <?php //NOTE: We�ll show all categories form database ?>
        
        <?php while ($cat = $categories -> fetch_assoc()): ?>
            <li><a href="posts.php?category=<?php echo $cat['id']?>"><?php echo $cat['name']?></a></li>
           
        <?php endwhile;?>
        </ol>
        <?php else : ?>
        <p> There are no categories yet</p>
        <?php endif;?>
            
        
    </div>

    <!-- /.blog-sidebar -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->
</div>

<?php include 'includes/footer.php';?>