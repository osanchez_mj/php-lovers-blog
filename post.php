
<?php include 'config/config.php';?>
<?php include 'libraries/Database.php';?>
<?php include 'helpers/format_helper.php';?>
<?php include 'includes/header.php';?>
<?php 

$db = new Database();

//we get the current post via the get method and id
$query = "SELECT * FROM posts WHERE id = ". $_GET['id'];

$result = $db -> select($query);

$selectedPost = $result -> fetch_assoc();

//query for categories table

$query = "SELECT * FROM categories";

//Get correct data

$categories= $db -> select($query);



?>
   <div class="container">
    <div class="row">

        <div class="col-sm-8 blog-main">
            <div class="blog-post">
                <h2 class="blog-post-title"><?php echo $selectedPost['title']?></h2>
                <p class="blog-post-meta"><?php echo formatDate($selectedPost['date']); ?> by <a href="#"><?php echo $selectedPost['author']?></a></p>
                <p class="text-justify">
                	<?php echo $selectedPost['body']?>
                </p>

            </div>
           
        </div>
        <!-- /.blog-main -->
        
        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    <div class="sidebar-module sidebar-module-inset">
        <?php echo $aboutContent; ?>
    </div>
    <div class="sidebar-module">
        <h4>Categories</h4>
        <?php if ($categories):?>
        <ol class="list-unstyled">
        
        <?php //NOTE: We�ll show all categories form database ?>
        
        <?php while ($cat = $categories -> fetch_assoc()): ?>
            <li><a href="#"><?php echo $cat['name']?></a></li>
           
        <?php endwhile;?>
        </ol>
        <?php else : ?>
        <p> There are no categories yet</p>
        <?php endif;?>
    </div>

    <!-- /.blog-sidebar -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->
</div>

<?php include 'includes/footer.php';?>