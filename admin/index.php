<?php include '../config/config.php';?>
    <?php include '../libraries/Database.php';?>
        <?php include '../helpers/format_helper.php';?>
            <?php include 'includes/header.php';?>
<?php

$db = new Database;

$query = "SELECT posts.*, categories.name FROM posts
         INNER JOIN categories
         ON posts.category = categories.id
         ORDER BY posts.title DESC";

$post = $db -> select($query);

//query for categories table
	
$query = "SELECT * FROM categories ORDER BY name DESC";
	
//Get correct data
	
$categories= $db -> select($query);


                

?>

                    <!----------------------- MENU ------------------->

                    <div class="blog-masthead">
                        <div class="container">
                            <nav class="blog-nav">
                                <a class="blog-nav-item active" href="index.php">Dashboard</a>
                                <a class="blog-nav-item" href="add_post.php">Add Post</a>
                                <a class="blog-nav-item" href="add_category.php">Add Category</a>
                                <a class="blog-nav-item pull-right" href="http://localhost/phpBlogLovers">Visit Blog</a>
                            </nav>
                        </div>
                    </div>

                    <!------------------------- Main Content ----------------------------------->

                    <div class="container">
                        <div class="blog-header">

                            <h2>Admin Area</h2>

                        </div>
                    </div>

                    <div class="container">

                        <!--------------------Shout Table ----------------------------------->
                        <div class="row">
                            <div class="col-sm-12 blog-main">
                                
                                <?php if(isset($_GET['msg'])): ?>
                                
                                    <div class="alert alert-success"><?php echo htmlentities($_GET['msg']) ?></div>
                                
                                <?php endif; ?>

                                <table class="table table-striped">
                                    <caption class="text-uppercase text-center">Shout Table</caption>
                                    <tr>
                                        <th>Post ID#</th>
                                        <th>Post title</th>
                                        <th>Category</th>
                                        <th>Author</th>
                                        <th>Date</th>
                                    </tr>

                                    <?php while ($row = $post->fetch_assoc()) :?>
                                        <tr>
                                            <td>
                                                <?php echo $row['id']?>
                                            </td>
                                            <td>
                                                <a href="edit_post.php?id=<?php echo $row['id']?>">
                                                    <?php echo $row['title']?>
                                                </a>
                                            </td>
                                            <td>
                                                <?php echo $row['name']?>
                                            </td>
                                            <td>
                                                <?php echo $row['author']?>
                                            </td>
                                            <td>
                                                <?php echo formatDate($row['date'])?>
                                            </td>
                                        </tr>
                                        <?php endwhile; ?>

                                </table>



                            </div>
                        </div>

                        <!--------------------Category Table ----------------------------------->
                        <div class="row">
                            <div class="col-sm-12 blog-main">

                                <table class="table table-striped">
                                    <caption class="text-uppercase text-center">Category Table</caption>
                                    <tr>
                                        <th>Category ID#</th>
                                        <th>Category Name</th>
                                    </tr>
                                    <?php while ($row = $categories->fetch_assoc()) :?>
                                        <tr>
                                            <td>
                                                <?php echo $row['id']?>
                                            </td>
                                            <td>
                                                <a href="edit_category.php?id=<?php echo $row['id']?>">
                                                    <?php echo $row['name']?>
                                                </a>
                                            </td>
                                            
                                        </tr>
                                        <?php endwhile; ?>

                                </table>



                            </div>
                        </div>
                    </div>

                    <?php include "includes/footer.php"?>