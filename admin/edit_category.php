<?php include '../config/config.php';?>
    <?php include '../libraries/Database.php';?>
            <?php include 'includes/header.php';?>
<?php 
$id = $_GET['id'];

$db = new Database;


$query = "SELECT * FROM categories where id=".$id;

//Get correct data

$category= $db -> select($query) -> fetch_assoc();


	if(isset($_POST['submit'])){
		//Assign Vars
		$name = mysqli_real_escape_string($db->link, $_POST['name']);
		
		//Simple Validation
		if($name == ''){
			//Set Error
			$error = 'Please fill out all required fields';
		} else {
			$query = "UPDATE categories 
					SET 
					name = '$name'
                    WHERE id =".$id;				;
			
			$update_row = $db->update($query);
		}
	}

    if(isset($_POST['delete'])){
        
        $query = "DELETE FROM categories where id = ".$id;
        $delete_row = $db->delete($query);
        
    }
?>

                        <!----------------------- MENU ------------------->

                        <div class="blog-masthead">
                            <div class="container">
                                <nav class="blog-nav">
                                    <a class="blog-nav-item" href="index.php">Dashboard</a>
                                    <a class="blog-nav-item" href="add_post.php">Add Post</a>
                                    <a class="blog-nav-item active" href="add_category.php">Add Category</a>
                                    <a class="blog-nav-item pull-right" href="http://localhost/phpBlogLovers">Visit Blog</a>
                                </nav>
                            </div>
                        </div>

                        <!------------------------- Main Content ----------------------------------->

                        <div class="container">
                            <div class="blog-header">

                                <h2>Add Category</h2>

                                <!--------------------------- Form --------------------------------------------->

                                <form class="form-horizontal" method="post" action="edit_category.php?id=<?php echo $id;?>">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Category Name</label>
                                        <div class="col-sm-4">
                                            <input name="name" type="text" class="form-control" placeholder="Enter Category" value="<?php echo $category['name']?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <input name="submit" type="submit" id="btn-color" class="btn mdl-button mdl-js-button mdl-js-ripple-effect" value="Submit" />
                                            <a href="index.php" class="btn btn-danger mdl-button mdl-js-button mdl-js-ripple-effect">Cancel</a>
                                            <input name="delete" type="submit" class="btn btn-warning mdl-button mdl-js-button mdl-js-ripple-effect" value="delete" />
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>

                        <div class="container">


                        </div>

                        <?php include "includes/footer.php"?>