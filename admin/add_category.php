<?php include '../config/config.php';?>
    <?php include '../libraries/Database.php';?>
        <?php include '../helpers/format_helper.php';?>
            <?php include 'includes/header.php';?>
<?php 

$db = new Database();

$query = "SELECT * FROM categories";

//Get correct data

$categories= $db -> select($query);


if (isset($_POST['submit'])) {
    //asign vars
    
    $name = mysqli_real_escape_string($db->link, $_POST['name']);
    //mysqli_real_scape_string evita que se inserten caracteres no deseados como  NUL (ASCII 0), \n, \r, \, ', ", y Control-Z.
   
    
    //Simple validation
    
    if($name == '' ){
        //set error
        $error = "Please fill out all required fields";
        
    }
    
    else{
        
        $query = "INSERT INTO categories (name)
                  VALUES ('$name')";
        
        $insert_row = $db->update($query);
    }
    
}

?>

                <!----------------------- MENU ------------------->

                <div class="blog-masthead">
                    <div class="container">
                        <nav class="blog-nav">
                            <a class="blog-nav-item" href="index.php">Dashboard</a>
                            <a class="blog-nav-item" href="add_post.php">Add Post</a>
                            <a class="blog-nav-item active" href="add_category.php">Add Category</a>
                            <a class="blog-nav-item pull-right" href="http://localhost/phpBlogLovers">Visit Blog</a>
                        </nav>
                    </div>
                </div>

                <!------------------------- Main Content ----------------------------------->

                <div class="container">
                    <div class="blog-header">

                        <h2>Add Category</h2>

                        <!--------------------------- Form --------------------------------------------->

                        <form class="form-horizontal" method="post" action="add_category.php">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Category Name</label>
                                <div class="col-sm-4">
                                    <input name="name" type="text" class="form-control" placeholder="Enter Category">
                                </div>
                            </div>                           
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <input name="submit" type="submit" id="btn-color" class="btn mdl-button mdl-js-button mdl-js-ripple-effect" value="Submit"/>
                                    <a href="index.php" class="btn btn-danger mdl-button mdl-js-button mdl-js-ripple-effect">Cancel</a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

                <div class="container">


                </div>

                <?php include "includes/footer.php"?>