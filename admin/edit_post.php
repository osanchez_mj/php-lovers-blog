<?php include '../config/config.php';?>
    <?php include '../libraries/Database.php';?>
        <?php include 'includes/header.php';?>
<?php 
$id = $_GET['id'];

$db = new Database;

//we get the current post via the get method and id
$query = "SELECT * FROM posts WHERE id = ".$id;

$selectedPost = $db -> select($query)->fetch_assoc();


//query for categories table

$query = "SELECT * FROM categories";

//Get correct data

$categories= $db -> select($query);

?>


<?php
	if(isset($_POST['submit'])){
		//Assign Vars
		$title = mysqli_real_escape_string($db->link, $_POST['title']);
		$body = mysqli_real_escape_string($db->link, $_POST['body']);
		$category = mysqli_real_escape_string($db->link, $_POST['category']);
		$author = mysqli_real_escape_string($db->link, $_POST['author']);
		$tags = mysqli_real_escape_string($db->link, $_POST['tags']);
		//Simple Validation
		if($title == '' || $body == '' || $category == '' || $author == ''){
			//Set Error
			$error = 'Please fill out all required fields';
		} else {
			$query = "UPDATE posts 
					SET 
					title = '$title',
					body = '$body',
					category = '$category',
					author = '$author',
					tags = '$tags' 
					WHERE id =".$id;
			
			$update_row = $db->update($query);
		}
	}

    if(isset($_POST['delete'])){
        
        $query = "DELETE FROM posts where id = ".$id;
        $delete_row = $db->delete($query);
        
    }
?>

                <!----------------------- MENU ------------------->

                <div class="blog-masthead">
                    <div class="container">
                        <nav class="blog-nav">
                            <a class="blog-nav-item" href="index.php">Dashboard</a>
                            <a class="blog-nav-item" href="add_post.php">Add Post</a>
                            <a class="blog-nav-item" href="add_category.php">Add Category</a>
                            <a class="blog-nav-item pull-right" href="http://localhost/phpBlogLovers">Visit Blog</a>
                        </nav>
                    </div>
                </div>

                <!------------------------- Main Content ----------------------------------->

                <div class="container">
                    <div class="blog-header">

                        <h2>Admin Area</h2>

                        <!--------------------------- Form --------------------------------------------->

                        <form class="form-horizontal" method="post" action="edit_post.php?id=<?php echo $id; ?>">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Post Title</label>
                                <div class="col-sm-8">
                                    <input name="title" type="text" class="form-control" placeholder="Write the post Title" value="<?php echo $selectedPost['title']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Post Body</label>
                                <div class="col-sm-8">
                                    <textarea name="body" class="form-control" rows="3" placeholder="Enter Post Body">
                                        <?php echo $selectedPost['body']?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Category</label>
                                <div class="col-sm-3">
                                    <select name="category" class="form-control">
                                       <?php while($row = $categories -> fetch_assoc()): ?>
                                       <?php 
                                        if($row ['id'] == $selectedPost['category']){
                                            
                                            $selected = 'selected';
                                        }
                                        else{
                                            
                                            $selected = '';
                                        }
                                            
                                            
                                        ?>
                                        <option value="<?php echo $row['id']; ?>" <?php echo $selected; ?>><?php echo $row['name']; ?></option>
                                       <?php endwhile; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Author</label>
                                <div class="col-sm-8">
                                    <input name="author" class="form-control" placeholder="Enter Author Name" value="<?php echo $selectedPost['author']?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Tags</label>
                                <div class="col-sm-8">
                                    <textarea name="tags" class="form-control" rows="3" placeholder="Enter Tags"><?php echo $selectedPost['tags']?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <input name="submit" type="submit" id="btn-color" class="btn mdl-button mdl-js-button mdl-js-ripple-effect" value="Submit"/>
                                    <a href="index.php" class="btn btn-danger mdl-button mdl-js-button mdl-js-ripple-effect">Cancel</a>
                                    <input name="delete" type="submit" class="btn btn-warning mdl-button mdl-js-button mdl-js-ripple-effect" value="delete"/>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

                <div class="container">


                </div>

                <?php include "includes/footer.php"?>