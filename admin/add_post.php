<?php include '../config/config.php';?>
    <?php include '../libraries/Database.php';?>
        <?php include '../helpers/format_helper.php';?>
            <?php include 'includes/header.php';?>
<?php 

$db = new Database();

$query = "SELECT * FROM categories";

//Get correct data

$categories= $db -> select($query);


if (isset($_POST['submit'])) {
    //asign vars
    
    $title = mysqli_real_escape_string($db->link, $_POST['title']);
    //mysqli_real_scape_string evita que se inserten caracteres no deseados como  NUL (ASCII 0), \n, \r, \, ', ", y Control-Z.
    $body = mysqli_real_escape_string($db->link, $_POST['body']);
    $category = mysqli_real_escape_string($db->link, $_POST['category']);
    $author = mysqli_real_escape_string($db->link, $_POST['author']);
    $tags = mysqli_real_escape_string($db->link, $_POST['tags']);
    
    //Simple validation
    
    if($title == '' || $body == '' || $category == '' || $author == '' || $tags == '' ){
        //set error
        $error = "Please fill out all required fields";
        
    }
    
    else{
        
        $query = "INSERT INTO posts (title, body, category, author, tags)
                  VALUES ('$title', '$body', '$category', '$author', '$tags')";
        
        $insert_row = $db->insert($query);
    }
    
}

?>

                    <!----------------------- MENU ------------------->

                    <div class="blog-masthead">
                        <div class="container">
                            <nav class="blog-nav">
                                <a class="blog-nav-item" href="index.php">Dashboard</a>
                                <a class="blog-nav-item active" href="add_post.php">Add Post</a>
                                <a class="blog-nav-item" href="add_category.php">Add Category</a>
                                <a class="blog-nav-item pull-right" href="http://localhost/phpBlogLovers">Visit Blog</a>
                            </nav>
                        </div>
                    </div>

                    <!------------------------- Main Content ----------------------------------->

                    <div class="container">
                        <div class="blog-header">

                            <h2>Add Post</h2>

                            <!--------------------------- Form --------------------------------------------->

                            <form class="form-horizontal" method="post" action="add_post.php">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Post Title</label>
                                    <div class="col-sm-8">
                                        <input name="title" type="text" class="form-control" placeholder="Write the post Title">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Post Body</label>
                                    <div class="col-sm-8">
                                        <textarea name="body" class="form-control" rows="3" placeholder="Enter Post Body"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Category</label>
                                    <div class="col-sm-3">
                                        <select name="category" class="form-control">
                                       <?php while($row = $categories -> fetch_assoc()): ?>
                                       <?php 
                                        if($row ['id'] == $selectedPost['category']){
                                            
                                            $selected = 'selected';
                                        }
                                        else{
                                            
                                            $selected = '';
                                        }
                                            
                                            
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo  $row['id']?>"><?php echo $row['name']?></option>
                                       <?php endwhile; ?>
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Author</label>
                                    <div class="col-sm-8">
                                        <input name="author" type="text" class="form-control" placeholder="Enter Author Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tags</label>
                                    <div class="col-sm-8">
                                        <textarea name="tags" class="form-control" rows="3" placeholder="Enter Tags"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-7">
                                        <input name="submit" type="submit" id="btn-color" class="btn mdl-button mdl-js-button mdl-js-ripple-effect" value="Submit" />
                                        <a href="index.php" class="btn btn-danger mdl-button mdl-js-button mdl-js-ripple-effect">Cancel</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="container">


                    </div>

                    <?php include "includes/footer.php"?>