<?php

class Database{
	
	public $host = DB_HOST;
	public $username = DB_USER;
	public $password = DB_PASS;
	public $db_name = DB_NAME;
	
	
	public $link;
	public $error;
	
	//constructor
	public function  __construct(){
		
	
		//llamamos a la funcion connect
		$this->connect();
	}
	
	//Conector
	
	private function connect(){
		
		//Aqui se realiza la conecci�n con mysql
		//el this es obligatorio
		$this->link = new mysqli($this->host, $this->username, $this->password, $this->db_name);//este es el api de musql
		
		if (! $this->link){
			
			$error = "error de coneccion con la base de datos: " . $this->link ->connect_error;
			
			return false;
			
		}
		
	}
	
	//Select 
	
	public function select ($query) {
		
		//link se refiere a el objeto de mysqli devuelto en la funci�n link
		$result = $this->link->query($query) or die ($this->link->error.__LINE__);
		
		if ($result -> num_rows > 0) {
			return $result;
		}
		else{
			
			return false;
		}
	}
	
	//insert y update solo se accederan desde admin
	
	//Insert
	public function insert($query) {
		
		$insert_row = $this->link->query($query) or die ($this->link->error.__LINE__);
		
		//Validar el insert
		//si es true entonces procedemos redireccionar
		if ($insert_row ) {
			header("Location: index.php?msg=".urlencode('Record Added'));
			exit();
		}
		else{
			
			die('Error: '. $this->link->errno . "  ". $this->link->error);
			
		}
	}

	//update
	public function update($query) {
	
		$update_row = $this->link->query($query) or die ($this->link->error.__LINE__);
	
		//Validar el insert
		//si es true entonces procedemos redireccionar
		if ($update_row ) {
			header("Location: index.php?msg=".urlencode('Record Updated'));
			exit();
		}
		else{
				
			die('Error: '. $this->link->errno . "  ". $this->link->error);
				
		}
	}
	
	//Delete
	public function delete($query) {
	
		$delete_row = $this->link->query($query) or die ($this->link->error.__LINE__);
	
		//Validar el insert
		//si es true entonces procedemos redireccionar
		if ($delete_row ) {
			header("Location: index.php?msg=".urlencode('Record Deleted'));
			exit();
		}
		else{
	
			die('Error: '. $this->link->errno . "  ". $this->link->error);
	
		}
	}
}
?>
